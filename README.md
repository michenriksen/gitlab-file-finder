# gitlab-file-finder

A simple tool to find files across projects belonging to a GitLab user or group.

## Usage

```
gitlab-file-finder -h
Usage of gitlab-file-finder:

gitlab-file-finder [flags] path1 [path2] ... [pathN]

FLAGS:

  -base-url string
    	Base URL for GitLab API (default "https://gitlab.com/")
  -debug
    	Print debugging information
  -group string
    	Group name to crawl
  -json
    	Print output in JSON format
  -subgroups
    	Include projects from subgroups (default true)
  -user string
    	Username to crawl
```

### Examples

#### Find all projects belonging to the `gitlab-org` group with `yarn.lock` file in project root:

    gitlab-file-finder -group gitlab-org yarn.lock

### Find all projects belonging to the `johndoe` user with `Gemfile.lock` file in project root:

    gitlab-file-finder -user johndoe Gemfile.lock

### Find all projects belonging to the `gitlab-org` group with either `yarn.lock` or `Gemfile.lock` file:

    gitlab-file-finder -group gitlab-org yarn.lock Gemfile.lock

### Find yarn.lock file and print output as JSON with debugging information:

    gitlab-file-finder -json -debug -group gitlab.org yarn.lock

## Installation

    go get -u gitlab.com/michenriksen/gitlab-file-finder

Set the `GITLAB_ACCESS_TOKEN` environment variable to a valid GitLab access token.