package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/xanzy/go-gitlab"
)

const accessTokenEnvVariable = "GITLAB_ACCESS_TOKEN"

type options struct {
	Group            *string
	User             *string
	Paths            []string
	IncludeSubgroups *bool
	BaseURL          *string
	OutputJSON       *bool
	Debug            *bool
}

func parseOptions() (options, error) {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n\n", os.Args[0])

		fmt.Fprintf(os.Stderr, "%s [flags] path1 [path2] ... [pathN]\n\n", os.Args[0])

		fmt.Fprint(os.Stderr, "FLAGS:\n\n")

		flag.PrintDefaults()
	}

	opts := options{
		Group:            flag.String("group", "", "Group name to crawl"),
		User:             flag.String("user", "", "Username to crawl"),
		IncludeSubgroups: flag.Bool("subgroups", true, "Include projects from subgroups"),
		BaseURL:          flag.String("base-url", "https://gitlab.com/", "Base URL for GitLab API"),
		OutputJSON:       flag.Bool("json", false, "Print output in JSON format"),
		Debug:            flag.Bool("debug", false, "Print debugging information"),
	}
	flag.Parse()

	opts.Paths = flag.Args()
	return opts, nil
}

func initLogger(opts options) {
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	if *opts.Debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	if !*opts.OutputJSON {
		out := zerolog.NewConsoleWriter(
			func(w *zerolog.ConsoleWriter) {
				w.TimeFormat = "15:04:05"
			},
		)
		log.Logger = log.Output(out)
	}
}

func getGroupProjects(name string, includeSubgroups *bool, client *gitlab.Client, logger zerolog.Logger) ([]gitlab.Project, error) {
	logger = logger.With().Str("group", name).Logger()
	allProjects := make([]gitlab.Project, 0)
	opts := &gitlab.ListGroupProjectsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
		IncludeSubgroups: includeSubgroups,
	}
	for {
		logger.Debug().Msgf("Retrieving projects page %d...", opts.Page)
		projects, resp, err := client.Groups.ListGroupProjects(name, opts)
		if err != nil {
			return allProjects, err
		}
		for _, project := range projects {
			// Ugly way to filter out projects that don't directly belong to the group... :/
			if !strings.Contains(project.WebURL, fmt.Sprintf("/%s/", name)) {
				logger.Debug().Msgf("%s does not belong to group", project.WebURL)
				continue
			}
			allProjects = append(allProjects, *project)
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opts.Page = resp.NextPage
	}
	return allProjects, nil
}

func getUserProjects(name string, client *gitlab.Client, logger zerolog.Logger) ([]gitlab.Project, error) {
	logger = logger.With().Str("user", name).Logger()
	allProjects := make([]gitlab.Project, 0)
	opts := &gitlab.ListProjectsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}
	for {
		logger.Debug().Msgf("Retrieving projects page %d...", opts.Page)
		projects, resp, err := client.Projects.ListUserProjects(name, opts)
		if err != nil {
			return allProjects, err
		}
		for _, project := range projects {
			allProjects = append(allProjects, *project)
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opts.Page = resp.NextPage
	}
	return allProjects, nil
}

func main() {
	opts, err := parseOptions()
	if err != nil {
		panic(err)
	}
	initLogger(opts)
	if len(opts.Paths) == 0 {
		log.Fatal().Msg("Please specify at least one path.")
	}
	if *opts.Group == "" && *opts.User == "" {
		log.Fatal().Msg("Please specify a group or user to crawl.")
	}
	if os.Getenv(accessTokenEnvVariable) == "" {
		log.Fatal().Msgf("Environment variable %s is not set. Please set to a valid GitLab access token.", accessTokenEnvVariable)
	}

	client, err := gitlab.NewClient(os.Getenv(accessTokenEnvVariable), gitlab.WithBaseURL(*opts.BaseURL))
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to initialize GitLab API client.")
	}

	var projects []gitlab.Project
	if *opts.Group != "" {
		p, err := getGroupProjects(*opts.Group, opts.IncludeSubgroups, client, log.Logger)
		if err != nil {
			log.Fatal().Err(err).Msgf("Failed to get projects for group %s", *opts.Group)
		}
		projects = p
	} else {
		p, err := getUserProjects(*opts.User, client, log.Logger)
		if err != nil {
			log.Fatal().Err(err).Msgf("Failed to get projects for user %s", *opts.Group)
		}
		projects = p
	}
	log.Debug().Msgf("Retrieved %d projects", len(projects))

	for _, project := range projects {
		ref := project.DefaultBranch
		if ref == "" {
			log.Debug().Msgf("No default branch on %s - setting to ref to 'master'", project.WebURL)
			ref = "master"
		}
		for _, path := range opts.Paths {
			file, _, err := client.RepositoryFiles.GetFile(project.ID, path, &gitlab.GetFileOptions{Ref: &ref})
			if err != nil {
				log.Debug().Err(err).Msgf("Error when retrieving path %s on %s", path, project.WebURL)
				continue
			}
			log.Info().Int("project_id", project.ID).Int("size", file.Size).Str("sha256", file.SHA256).Msgf("%s/-/blob/%s/%s", project.WebURL, ref, path)
		}
	}
}
