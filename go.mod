module gitlab.com/michenriksen/gitlab-file-finder

go 1.15

require (
	github.com/rs/zerolog v1.20.0
	github.com/xanzy/go-gitlab v0.42.0
)
